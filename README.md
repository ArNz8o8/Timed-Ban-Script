Timed ban with a change to escape it by guessing the right code phrase
--------------------------------------------------------------------------------
To start the timed ban a user must type:
   !troll <nickname>
Selecting the code phrase is done by typing:
  !code <codephrase>

  The codes you can choose from are displayed when the ban is activated.
  
  This script will not allow bots (Users who are +b), or the bot running the
  script, or friends of the bot (+g flag needed) to be banned.



  (C) 2016 ArNz|8o8 - Based on timebomb.tcl by http://radiosaurus.sporkism.org
  
  
  
  Made for eggdrop 1.6.x and up (Tested on CVS1.8.0/TCL8.6)

  